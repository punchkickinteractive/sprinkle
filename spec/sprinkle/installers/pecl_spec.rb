require File.expand_path("../../spec_helper", File.dirname(__FILE__))

describe Sprinkle::Installers::Pecl do

  before do
    @package = mock(Sprinkle::Package, :name => 'spec')
    @installer = Sprinkle::Installers::Pecl.new(@package, 'spec')
  end

  describe 'during installation' do
    it 'should invoke the pecl executer for all specified tasks' do
      @install_commands = @installer.send :install_commands
      @install_commands.should == "pecl install spec"
    end
  end
end
