module Sprinkle
  module Verifiers
    # = Pecl package Verifier
    #
    # Contains a verifier to check the existence of a Pecl package.
    # 
    # == Example Usage
    #
    #   verify { has_pecl 'PHP_Compat' }
    #
    module Pecl
      Sprinkle::Verify.register(Sprinkle::Verifiers::Pecl)

      # Checks to make sure the pecl <tt>package</tt> exists on the remote server.
      def has_pear(package)
        @commands << "pecl list | grep \"#{package}\" | grep \"stable\""
      end

    end
  end
end